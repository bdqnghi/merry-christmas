package com.hksoft.merrychristmas;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;
import com.hksoft.merrychristmas.R;

import android.app.Activity;
import android.app.WallpaperManager;
import android.content.ComponentName;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class WallpaperActivity extends Activity {
	private InterstitialAd mInterstitial;
	private AdView mAdView;
    private Button setWallpaperButton;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.act_wallpaper);
        mAdView = (AdView) findViewById(R.id.adView);
		mAdView.loadAd(new AdRequest.Builder().build());
		mInterstitial = new InterstitialAd(this);
		mInterstitial
				.setAdUnitId(getResources().getString(R.string.ad_unit_id));

		mInterstitial.loadAd(new AdRequest.Builder().build());


        setWallpaperButton = (Button) findViewById(R.id.btn_set_wallpaper);
        setWallpaperButton.setOnClickListener(setWallpaperListener);

    }

    private View.OnClickListener setWallpaperListener = new View.OnClickListener() {

        @Override
        public void onClick(View v) {

        	Intent intent = new Intent(
					WallpaperManager.ACTION_LIVE_WALLPAPER_CHOOSER);
			intent.putExtra(WallpaperManager.ACTION_LIVE_WALLPAPER_CHOOSER,
					new ComponentName(WallpaperActivity.this,
							Wallpaper.class));
			startActivity(intent);
			Toast.makeText(WallpaperActivity.this, "Christmas Live Wallpaper",
					Toast.LENGTH_SHORT).show();
			if (mInterstitial.isLoaded()) {
				mInterstitial.show();
			}

        }
    };
    public void loadInterstitial() {

		mInterstitial.loadAd(new AdRequest.Builder().build());
	}

	public void showInterstitial() {
		if (mInterstitial.isLoaded()) {
			mInterstitial.show();
		}
	}
    public void btnOpenActivity (View view){
       
    }
    
    @Override
	public void onResume() {
		super.onResume();
		mAdView.resume();
		// startAppAd.onResume();
		if (mInterstitial != null && mInterstitial.isLoaded()) {
			mInterstitial.show();
		} else {
			loadInterstitial();
			showInterstitial();
		}
	}

	@Override
	public void onPause() {

		mAdView.pause();

		if (mInterstitial != null && mInterstitial.isLoaded()) {
			mInterstitial.show();
		} else {
			loadInterstitial();
			showInterstitial();
		}
		super.onPause();
	}

	@Override
	public void onBackPressed() {
		// startAppAd.onBackPressed();
		if (mInterstitial != null && mInterstitial.isLoaded()) {
			mInterstitial.show();
		} else {
			mInterstitial.loadAd(new AdRequest.Builder().build());
			mInterstitial.show();

		}
		super.onBackPressed();
	}

	@Override
	protected void onDestroy() {
		mAdView.destroy();
		super.onDestroy();
	}
}